import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide=true;
  public errorMessage:string;
  email:string;
  password:string;

  constructor(public authService:AuthService,private router:Router) {
    this.authService.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
    authService.err = "";
   }
  

  onSubmit(){
    this.authService.login(this.email,this.password);
  }

  ngOnInit() {
  }

}
