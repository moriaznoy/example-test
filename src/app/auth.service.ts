import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>
  private logInErrorSubject = new Subject<string>();
  public err;

  getUser(){
    return this.user
  }

  constructor(public afAuth:AngularFireAuth,private router:Router) {
    this.user = this.afAuth.authState; 
   }
 

  signup(email:string, passwoerd:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,passwoerd)
        .then(res =>  {
                      console.log('Succesful sign up',res);
                      this.router.navigate(['/welcome']);
                    }) 
    .catch(
      error => this.err = error
    ) 
  }

  logout(){
    this.afAuth.auth.signOut()
    .then(res => {
      console.log('Successful logout',res)
      this.router.navigate(['/welcome']);

    });
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/welcome']);
            }     
        )
    .catch(
       error => this.err = error
    )
  }

   public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }


}