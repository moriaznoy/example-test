import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  APIposts = "https://jsonplaceholder.typicode.com/posts"
  APIcomments = "https://jsonplaceholder.typicode.com/comments"

  uCollection:AngularFirestoreCollection = this.database.collection('users');
  pCollection:AngularFirestoreCollection;

  constructor(private http: HttpClient, private database:AngularFirestore, public auth:AuthService) { }


  
  deletepost(userId:string,id:string){
    this.database.doc(`users/${userId}/posts/${id}`).delete();
  }
  
  addPost(userId:string, title:string, body:string){
      const post = {title:title, body:body, like:0}; //zero the likes to start
      this.uCollection.doc(userId).collection('posts').add(post);
      }

  updateLikesInAPost(userId:string, id:string,like:number){
      this.database.doc(`users/${userId}/posts/${id}`).update({like:like})
       }

  getThePost(userId, id:string):Observable<any>{
      return this.database.doc(`users/${userId}/posts/${id}`).get();
      }
  
  getUserPosts(userId:string):Observable<any[]>{
    this.pCollection = this.database.collection(`users/${userId}/posts`);
      console.log('posts collection created');
        return this.pCollection.snapshotChanges().pipe(
            map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
              return { ...data };
              }))
            ); 
         }
 
  getPost():Observable<any>{
      return this.http.get<Post[]>(this.APIposts);
      }

  getComments():Observable<any>{
    return this.http.get<Comment[]>(this.APIcomments);

  }
      
}
