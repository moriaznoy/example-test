import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { PostService } from '../post.service';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {
  
  posts$:Observable<any>;
  userId:string;
  like:number;

  constructor(public auth:AuthService, private postserice:PostService ) { }

  delete(id:string){
    this.postserice.deletepost(this.userId,id);
  }

  addlike(id:string,likes:number){
    this.like = likes+1; 
    this.postserice.updateLikesInAPost(this.userId, id, this.like)
  }
  ngOnInit() {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
    this.posts$ = this.postserice.getUserPosts(this.userId);
       }
    )
  }
}
