import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { Post } from '../interfaces/post';
import { AuthService } from '../auth.service';
import { Comment } from '../interfaces/comment';
import { Observable } from 'rxjs';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-blogposts',
  templateUrl: './blogposts.component.html',
  styleUrls: ['./blogposts.component.css']
})
export class BlogpostsComponent implements OnInit {
  
  Posts$:Observable<Post[]>;
  comments$:Observable<Comment[]>;
  userId:string;
  text:string;
  postid:number;

  constructor(private postService:PostService,private auth:AuthService) { }

  add(title:string, body:string, id:number){
        this.postService.addPost(this.userId, title, body)
        this.postid = id;
        this.text = "Saved for later viewing"
      }

  ngOnInit() {
    this.postService.getPost().subscribe(data => this.Posts$ = data );
    this.postService.getComments().subscribe(data => this.comments$ = data );
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }
}
